import { RouterModule, Routes } from '@angular/router';


//componentes
import {TiendaComponent} from './components/tienda/tienda.component';
import {HomeComponent} from './components/home/home.component';
import {AnimalsComponent} from './components/animals/animals.component';
import {KeepersComponent} from './components/keepers/keepers.component';
import {ParquesComponent} from './components/parques/parques.component';
import {ContactComponent} from './components/contact/contact.component';


const APP_ROUTES: Routes = [
  //{ path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'animales', component: AnimalsComponent },
  { path: 'contacto', component: ContactComponent },
  { path: 'cuidadores', component: KeepersComponent },
  { path: 'tienda', component: TiendaComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

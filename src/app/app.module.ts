import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
//importar nuestro nuevo modulo
import {ModuloemailModule} from './moduloemail/moduloemail.module';
import {AdminModule} from './admin/admin.module';
//rutas
import {APP_ROUTING} from './app.routing';

//componentes
import { AppComponent } from './app.component';
import { TiendaComponent } from './components/tienda/tienda.component';
import { ParquesComponent } from './components/parques/parques.component';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { AnimalsComponent } from './components/animals/animals.component';
import { KeepersComponent } from './components/keepers/keepers.component';


@NgModule({
  //se carga los nuevo pipes y componentes
  declarations: [
    AppComponent,
    TiendaComponent,
    ParquesComponent,
    HomeComponent,
    ContactComponent,
    AnimalsComponent,
    KeepersComponent
  ],
  //se carga los modulo internos o customizado
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    ModuloemailModule,
    AdminModule
  ],
  //servicios
  providers: [

  ],
  //el componente principal
  bootstrap: [AppComponent]
})
export class AppModule { }

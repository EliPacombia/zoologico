import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-guardar-email',
  templateUrl: './guardar-email.component.html'
})
export class GuardarEmailComponent implements OnInit {
  title = 'Guardar email';
  //creamos esta variable para la visualizacion
  emailContacto:string;
  //utilizamos el localStorage para guardar los datos de emailContacto
  guardarEmail(){
  localStorage.setItem('emailContacto',this.emailContacto);
  //console.log(localStorage.getItem('emailContacto'));
  }
  ngOnInit(){
    
  }
}

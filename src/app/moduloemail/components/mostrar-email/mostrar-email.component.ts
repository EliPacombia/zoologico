import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mostrar-email',
  templateUrl: './mostrar-email.component.html',
  styles: []
})
export class MostrarEmailComponent implements OnInit {

   title = 'Mostrar email';
    //creamos esta variable para la visualizacion
    emailContacto:string;
    //utilizamos el localStorage para guardar los datos de emailContacto
    ngOnInit(){
      this.emailContacto = localStorage.getItem('emailContacto');
    //  console.log(localStorage.getItem('emailContacto'));
    }
    //el DoCheck actualiza tus datos en tiempo real
    ngDoCheck(){
      this.emailContacto = localStorage.getItem('emailContacto');
    }
    //eliminar y limpiar datos con esta funcion del localStorage
    borrarEmail(){
      localStorage.removeItem('emailContacto');
      localStorage.clear();
      this.emailContacto=null;
    }

}

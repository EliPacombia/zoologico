//Imprtar Modulos neceario para crear modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';

//importar componentes
import {GuardarEmailComponent} from './components/guardar-email/guardar-email.component';
import {MainEmailComponent} from './components/main-email/main-email.component';
import {MostrarEmailComponent} from './components/mostrar-email/mostrar-email.component';

//decorador ngmodule para cargar los componentes y la configuracion del modulos
@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    GuardarEmailComponent,
    MainEmailComponent,
    MostrarEmailComponent
  ],
  exports:[MainEmailComponent]
})
export class ModuloemailModule { }

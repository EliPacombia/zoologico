import { Component, OnInit,
   Input,Output, EventEmitter
   ,OnChanges, SimpleChanges,
   OnDestroy, DoCheck} from '@angular/core';

@Component({
  selector: 'app-parques',
  templateUrl: './parques.component.html',
  styleUrls: ['./parques.component.css']
})
export class ParquesComponent implements OnInit,DoCheck, OnChanges,OnDestroy {
@Input() public nombre: string;
@Input('metros_cuadrados')  metros:number;
public vegetacion:string;
public abierto:boolean;

@Output() pasameLosDatos = new EventEmitter();
  constructor()
  {
    /**declaramos los valeres  de las variables declarados **/
    this.nombre = 'Parque natural para caballos';
    this.metros = 450;
    this.vegetacion='Alta';
    this.abierto=false;
   }
  /*** este metodo se ejecuta cuadno se carga la direcctiva o etiquta de parque **/
  ngOnInit() {
    console.log('MEtodo on init lanzado');
  }
  /**metodo para comprobar cosas cada ves que se hace un cambio o movimientos
  se lanza depues de ngOnInit y el OnChange **/
  ngDoCheck(){
    console.log('El Docheck se ha ejecutado');
  }
  /*** este metodo te muestra los cambio que realizas **/
  ngOnChanges(changes: SimpleChanges){
    //console.log(changes);
    console.log('Existen Cambio en las Propiedades');
  }
  /** se ejecuta antes de que la variable, o se destrue el componente **/
  ngOnDestroy(){
    console.log('Se va a eliminar el Componente');
  }
  /** metodo del voton emiter, para mostrar los datos en el padre **/
  emitirEvento(){
    this.pasameLosDatos.emit({
    'nombre':this.nombre,
    'metros':this.metros,
    'vegetacion':this.vegetacion,
    'abierto':this.abierto
    });
  }

}

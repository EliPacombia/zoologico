import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  title="Componente Contacto";
  emailContacto:string;

  constructor() { }

  ngOnInit() {
    console.log('Contacto');
  }
  guardarEmail(){
  localStorage.setItem('emailContacto',this.emailContacto);
  console.log(localStorage.getItem('emailContacto'));
  }
}
